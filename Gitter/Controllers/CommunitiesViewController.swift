import Foundation
import CoreData

class CommunitiesViewController: UITableViewController, HintViewDelegate {

    private let restService = RestService()
    private let notificationCenter = NotificationCenter.default
    private let managedObjectContext = CoreDataSingleton.sharedInstance.uiManagedObjectContext
    private var hintView: HintView?
    private var viewWhenEmpty: UIView?
    private var groupsWithMetadata = [GroupWithMetadata]() {
        didSet {
            tableView.reloadData()
        }
    }

    override func loadView() {
        super.loadView()

        let bundle = Bundle.main
        hintView = bundle.loadNibNamed("HintView", owner: self, options: nil)!.first as? HintView
        hintView?.button.setTitle("Find a room to join a Community!", for: UIControlState())
        hintView?.delegate = self
        viewWhenEmpty = bundle.loadNibNamed("LoadingView", owner: self, options: nil)!.first as? UIView
    }

    deinit {
         notificationCenter.removeObserver(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "AvatarWithBadgeCell", bundle: nil), forCellReuseIdentifier: "AvatarWithBadgeCell")

        tableView.backgroundView = viewWhenEmpty
        tableView.tableFooterView = UIView()

        notificationCenter.addObserver(self, selector: #selector(self.managedObjectContextDidSave), name: NSNotification.Name.NSManagedObjectContextDidSave, object: managedObjectContext)

        query { (groupsWithMetadata) in
            self.groupsWithMetadata = groupsWithMetadata
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // groups arent updated via faye, so we need to fetch them manually
        restService.getUserGroups { (error, didSaveNewData) in
            LogIfError("Failed fetch user groups", error: error)
            self.viewWhenEmpty = self.hintView
            if (self.tableView.backgroundView != nil) {
                self.tableView.backgroundView = self.viewWhenEmpty
            }
        }
    }

    @IBAction func didPullToRefresh(_ sender: UIRefreshControl) {
        restService.getUserGroups { (error, didSaveNewData) in
            LogIfError("Failed refresh user groups", error: error)
            sender.endRefreshing()
            self.viewWhenEmpty = self.hintView
            if (self.tableView.backgroundView != nil) {
                self.tableView.backgroundView = self.viewWhenEmpty
            }
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRows = groupsWithMetadata.count
        tableView.backgroundView = numberOfRows == 0 ? viewWhenEmpty : nil
        return numberOfRows
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvatarWithBadgeCell", for: indexPath) as! AvatarWithBadgeCell
        configureCell(cell, indexPath: indexPath)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "GroupRoomListSegue", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? GroupRoomListViewController {
            let group = groupsWithMetadata[(tableView.indexPathForSelectedRow! as NSIndexPath).row].group
            vc.groupId = group.id
            vc.title = group.name
        }
    }

    func didTapButton(_ hintView: HintView, button: UIButton) {
        (tabBarController as! GitterTabBarController).switchTabs(.search)
    }

    func managedObjectContextDidSave(_ notification: Notification) {
        if let userInfo = (notification as NSNotification).userInfo {
            let insertedObjects = userInfo[NSInsertedObjectsKey] as? Set<NSManagedObject> ?? []
            let updatedObjects = userInfo[NSUpdatedObjectsKey] as? Set<NSManagedObject> ?? []
            let deletedObjects = userInfo[NSDeletedObjectsKey] as? Set<NSManagedObject> ?? []

            if isRequeryRequired(insertedObjects: insertedObjects, updatedObjects: updatedObjects, deletedObjects: deletedObjects) {
                query({ (groupsWithMetadata) in
                    self.groupsWithMetadata = groupsWithMetadata
                })
            }
        }
    }

    private func configureCell(_ cell: AvatarWithBadgeCell, indexPath: IndexPath) {
        let groupWithMetadata = groupsWithMetadata[(indexPath as NSIndexPath).row]

        AvatarUtils.sharedInstance.configure(cell.avatar, group: groupWithMetadata.group, size: AvatarWithBadgeCell.avatarHeight)

        cell.mainLabel.text = groupWithMetadata.group.name
        cell.sideLabel.text = nil
        cell.bottomLabel.text = nil
        if (groupWithMetadata.mentionedRooms > 0) {
            cell.badgeLabel.text = "@"
            cell.badgeType = .mention
        } else if (groupWithMetadata.unreadRooms > 0) {
            cell.badgeLabel.text = String(groupWithMetadata.unreadRooms)
            cell.badgeType = .unread
        } else {
            cell.badgeLabel.text = nil
            cell.badgeType = .none
        }
    }

    private func query(_ completionHandler: @escaping ([GroupWithMetadata]) -> Void) {
        let fetchRequest = NSFetchRequest<Group>(entityName: "Group")
        fetchRequest.predicate = NSPredicate(format: "userGroupCollection != NULL")
        // we're going to be sorting on each group's room list, so prefetch so that 1000 faults dont get fired
        fetchRequest.relationshipKeyPathsForPrefetching = ["rooms"]

        try! managedObjectContext.execute(NSAsynchronousFetchRequest(fetchRequest: fetchRequest, completionBlock: { (result) in
            // still in the background thread right now, lets do some processing
            let groupsWithMetadata = result.finalResult!.map({ (group) -> GroupWithMetadata in
                return GroupWithMetadata(group: group)
            }).sorted(by: { (a, b) -> Bool in
                if a.mentionedRooms == b.mentionedRooms {
                    if a.unreadRooms == b.unreadRooms {
                        if let aLast = a.lastAccessedRoomTime, let bLast = b.lastAccessedRoomTime {
                            return aLast.compare(bLast) == .orderedDescending
                        } else {
                            return a.lastAccessedRoomTime != nil
                        }
                    } else {
                        return a.unreadRooms > b.unreadRooms
                    }
                } else {
                    return a.mentionedRooms > b.mentionedRooms
                }
            })

            DispatchQueue.main.async(execute: {
                // back in the main thread
                completionHandler(groupsWithMetadata)
            })
        }))
    }

    private func isRequeryRequired(insertedObjects: Set<NSManagedObject>, updatedObjects: Set<NSManagedObject>, deletedObjects: Set<NSManagedObject>) -> Bool {
        return insertedObjects.union(updatedObjects).union(deletedObjects).contains(where: { (managedObject) -> Bool in
            // we can technically get finer grained than this (e.g ignore room inserts that are not in a group),
            // but its better to be simple and slightly too eager. Stuck unread badges suck.
            let name = managedObject.entity.name
            return name == "Room" || name == "Group"
        })
    }

    private class GroupWithMetadata {
        let group: Group
        var unreadRooms: Int = 0
        var mentionedRooms: Int = 0
        var lastAccessedRoomTime: Date?

        init(group: Group) {
            self.group = group

            self.group.rooms.forEach { (room) in
                if room.intUnreadItems > 0 {
                    unreadRooms += 1
                }
                if room.intMentions > 0 {
                    mentionedRooms += 1
                }
                if let lastAccessTime = room.lastAccessTime {
                    lastAccessedRoomTime = (lastAccessedRoomTime as NSDate?)?.laterDate(lastAccessTime as Date) ?? lastAccessTime as Date
                }
            }
        }
    }
}
