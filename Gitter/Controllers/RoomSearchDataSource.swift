import Foundation
import CoreData

class RoomSearchDataSource: NSObject, UITableViewDataSource {

    var delegate: RoomSearchDataSourceDelegate?

    private let restService = RestService()
    private let context = CoreDataSingleton.sharedInstance.uiManagedObjectContext
    private var results: [Room] = [] {
        didSet {
            delegate?.roomSearchResultsDidChange(self)
        }
    }

    private var previousQuery = ""

    func search(_ query: String) {
        guard query != previousQuery else {
            return
        }

        guard query.characters.count > 0 else {
            results = []
            previousQuery = query
            return
        }

        // query db as it should be faster than network.
        let fetchRequest = NSFetchRequest<Room>(entityName: "Room")
        fetchRequest.predicate = NSPredicate(format: "userRoomCollection != NULL AND oneToOne == FALSE AND (name CONTAINS[cd] %@ OR uri CONTAINS[cd] %@)", query, query)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "userRoomCollection", ascending: false)]
        asyncFetch(fetchRequest) { (rooms) in
            if (query == self.previousQuery) {
                self.results = rooms
            }
        }

        NetworkIndicator.sharedInstance.visible = true
        // query network in parallel with db search.
        // sort ordering puts db results first so rooms shouldnt jump about.
        // technically there is a race condition if this finishes before vanilla db search, but that would be damn impressive.
        restService.searchRooms(query) { (error, ids) in
            NetworkIndicator.sharedInstance.visible = false
            guard let ids = ids , error == nil else {
                return LogError("failed to search rooms via network", error: error!)
            }

            let fetchRequest = NSFetchRequest<Room>(entityName: "Room")
            fetchRequest.predicate = NSPredicate(format: "id IN %@ OR (userRoomCollection != NULL AND oneToOne == FALSE AND (name CONTAINS[cd] %@  OR uri CONTAINS[cd] %@))", ids, query, query)
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "userRoomCollection", ascending: false)]
            self.asyncFetch(fetchRequest) { (rooms) in
                if (query == self.previousQuery) {
                    self.results = rooms
                }
            }
        }

        previousQuery = query
    }

    func isEmpty() -> Bool {
        return results.isEmpty
    }

    func getItemAtIndex(_ indexPath: IndexPath) -> Room {
        return results[(indexPath as NSIndexPath).row]
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvatarWithBadgeCell", for: indexPath) as! AvatarWithBadgeCell
        let room = getItemAtIndex(indexPath)

        AvatarUtils.sharedInstance.configure(cell.avatar, room: room, size: AvatarWithBadgeCell.avatarHeight)
        cell.mainLabel.text = room.name
        cell.sideLabel.text = room.oneToOne ? "@" + room.uri : nil
        cell.bottomLabel.text = nil
        if (room.intMentions > 0) {
            cell.badgeType = .mention
            cell.badgeLabel.text = "@"
        } else if (room.intUnreadItems > 0) {
            cell.badgeType = .unread
            cell.badgeLabel.text = room.intUnreadItems > 99 ? "99+" : String(room.intUnreadItems)
        } else if (room.activity && room.lurk) {
            cell.badgeType = .activityIndicator
            cell.badgeLabel.text = nil
        } else {
            cell.badgeType = .none
            cell.badgeLabel.text = nil
        }

        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }

    private func asyncFetch(_ fetchRequest: NSFetchRequest<Room>, completionHandler: @escaping ([Room]) -> Void) {
        try! CoreDataSingleton.sharedInstance.uiManagedObjectContext.execute(NSAsynchronousFetchRequest(fetchRequest: fetchRequest, completionBlock: { (result) in
            let rooms = result.finalResult!
            DispatchQueue.main.async(execute: {
                completionHandler(rooms)
            })
        }))
    }
}

protocol RoomSearchDataSourceDelegate {
    func roomSearchResultsDidChange(_ roomSearchDataSource: RoomSearchDataSource)
}
