import UIKit
import WebKit

class LoginOauthViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    var authProvider: String?
    private let appSettings = TRAppSettings.sharedInstance()
    private var webView: WKWebView?
    private let loginData = LoginData()
    private let session =  URLSession(
        configuration: URLSessionConfiguration.ephemeral,
        delegate:nil,
        delegateQueue:OperationQueue.main
    )
    private let host = "https://gitter.im"

    override func viewDidLoad() {
        super.viewDidLoad()

        ClearCookies {
            let webView = WKWebView()

            webView.isHidden = true
            webView.scrollView.bounces = false
            webView.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(webView)
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[webView]|", options: [], metrics: nil, views: ["webView": webView]))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[topLayoutGuide][webView]|", options: [], metrics: nil, views: ["topLayoutGuide": self.topLayoutGuide, "webView": webView]))
            webView.navigationDelegate = self
            webView.addObserver(self, forKeyPath: "loading", options: .new, context: nil)
            self.webView = webView

            webView.load(URLRequest(url: self.getAuthorisationUrl()))
        }
    }

    deinit {
        webView?.removeObserver(self, forKeyPath: "loading")
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let url = navigationAction.request.url!

        if (url.absoluteString.hasPrefix(appSettings!.oauthScope())) {
            decisionHandler(.cancel)
            if let code = getCodeFromCallbackUrl(url) {
                exchangeTokens(code, completionHandler: { (accessToken) -> Void in
                    self.getUserId(accessToken, completionHandler: { (userId) -> Void in
                        LoginData().setLoggedIn(userId, withToken: accessToken)
                        NotificationCenter.default.post(name: Notification.Name(rawValue: TroupeAuthenticationReady), object: self)
                        self.showInitialViewController()
                    })

                })
            }
        } else {
            decisionHandler(.allow)
        }
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (keyPath == "loading") {
            if (!webView!.isLoading) {
                loadingIndicator.stopAnimating()
                webView!.isHidden = false
            }
        }
    }

    private func getAuthorisationUrl() -> URL {
        var components = URLComponents(string: "\(host)/login/oauth/authorize")!
        var queryItems = [
            URLQueryItem(name: "client_id", value: appSettings!.clientID()),
            URLQueryItem(name: "redirect_uri", value: appSettings!.oauthScope()),
            URLQueryItem(name: "response_type", value: "code"),
            URLQueryItem(name: "action", value: "login"),
            URLQueryItem(name: "source", value: "ios_login-login")
        ]
        if let authProvider = self.authProvider {
            queryItems.append(URLQueryItem(name: "auth_provider", value: authProvider))
        }
        components.queryItems = queryItems

        return components.url!
    }

    private func getCodeFromCallbackUrl(_ url: URL) -> String? {
        return URLComponents(url: url, resolvingAgainstBaseURL: true)?.queryItems?.filter({ (queryItem) -> Bool in
            return queryItem.name == "code"
        }).first?.value
    }

    private func exchangeTokens(_ code: String, completionHandler: @escaping (_ accessToken: String) -> Void) {
        let json = try! JSONSerialization.data(withJSONObject: [
            "client_id": appSettings!.clientID()!,
            "client_secret": appSettings!.clientSecret()!,
            "redirect_uri": appSettings!.oauthScope()!,
            "grant_type": "authorization_code",
            "code": code
            ], options: .prettyPrinted)

        var request = URLRequest(url: URL(string: "\(host)/login/oauth/token")!)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        request.httpBody = json

        session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            if let respJson = Api().parseIf200Ok(data, response: response, error: error) as? JsonObject {
                completionHandler(respJson["access_token"] as! String)
            }
            }) .resume()
    }

    private func getUserId(_ accessToken: String, completionHandler: @escaping (_ userId: String) -> Void) {
        let api = Api(accessToken: accessToken)
        api.authSession().dataTask(with: api.urlWithPath("/v1/user/me"), completionHandler: api.parseIf200Ok({ (json) -> Void in
            completionHandler((json as! JsonObject)["id"] as! String)
        })).resume()
    }

    private func showInitialViewController() {
        let storyboard = UIStoryboard(name: "MainStoryboard", bundle: Bundle.main)
        UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
    }
}
