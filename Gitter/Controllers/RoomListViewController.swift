import UIKit
import CoreData

class RoomListViewController: UITableViewController, NSFetchedResultsControllerDelegate, UIViewControllerPreviewingDelegate, CreateCommunityViewControllerDelegate, CreateRoomViewControllerDelegate, HintViewDelegate {

    private var fetchedResultsController: NSFetchedResultsController<Room>?
    private let notificationCenter = NotificationCenter.default
    private let bayeuxService = BayeuxService()
    private let managedObjectContext = CoreDataSingleton.sharedInstance.uiManagedObjectContext
    private var rootViewController: RootViewController?

    private var viewWhenEmpty: UIView?
    private var hintView: HintView?


    override func loadView() {
        super.loadView()

        let fetchRequest = NSFetchRequest<Room>(entityName: "Room")
        fetchRequest.predicate = NSPredicate(format: "userRoomCollection != NULL AND (lastAccessTime != NULL OR mentions > 0 OR unreadItems > 0)")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "favourite_comparable", ascending: true),
                                        NSSortDescriptor(key: "mentions", ascending: false),
                                        NSSortDescriptor(key: "unreadItems", ascending: false),
                                        NSSortDescriptor(key: "lastAccessTime", ascending:false)]
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController?.delegate = self

        notificationCenter.addObserver(self, selector: #selector(self.didFinishRefresh), name: NSNotification.Name(rawValue: "snapshotReceived"), object: nil)
        viewWhenEmpty = Bundle.main.loadNibNamed("LoadingView", owner: self, options: nil)!.first as? UIView
        hintView = Bundle.main.loadNibNamed("HintView", owner: self, options: nil)!.first as? HintView
        hintView?.button.setTitle("No Conversations?", for: UIControlState())
        hintView?.delegate = self
    }

    deinit {
        notificationCenter.removeObserver(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        rootViewController = self.splitViewController as? RootViewController

        tableView.register(UINib(nibName: "RoomListItemView", bundle: nil), forCellReuseIdentifier: "RoomCell")
        tableView.register(UINib(nibName: "AvatarWithBadgeCell", bundle: nil), forCellReuseIdentifier: "AvatarWithBadgeCell")

        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: view)
        }

        // remove infinite separators
        tableView.tableFooterView = UIView()



        try! fetchedResultsController?.performFetch()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            // this should happen automatically, but 1/10 times the row remains selected after navigating
            // back to this room list
            tableView.deselectRow(at: selectedIndexPath, animated: animated)
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRows = fetchedResultsController?.sections?[section].numberOfObjects ?? 0
        tableView.backgroundView = numberOfRows == 0 ? viewWhenEmpty : nil
        return numberOfRows
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvatarWithBadgeCell", for: indexPath) as! AvatarWithBadgeCell
        let room = fetchedResultsController!.object(at: indexPath)

        configureCell(cell, room: room)

        return cell
    }

    func didTapButton(_ hintView: HintView, button: UIButton) {
        (tabBarController as! GitterTabBarController).switchTabs(.search)
    }

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        TableViewChangeHandler.applyRowChange(to: tableView, at: indexPath, for: type, newIndexPath: newIndexPath)
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }

    // Create a previewing view controller to be shown at "Peek".
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = tableView.indexPathForRow(at: location), let cell = tableView.cellForRow(at: indexPath) else {
            return nil
        }

        previewingContext.sourceRect = cell.frame
        let room = fetchedResultsController!.object(at: indexPath)
        return rootViewController?.getRoomViewController(byId: room.id)
    }

    // Present the view controller for the "Pop" action.
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        // Reuse the "Peek" view controller for presentation.
        rootViewController?.showDetailViewController(viewControllerToCommit, sender: self)
    }

    // shared by our table view and the search results table view
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let room = fetchedResultsController!.object(at: indexPath)
        rootViewController?.navigateTo(roomId: room.id, sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let createCommunityVC = segue.destination as? CreateCommunityViewController {
            createCommunityVC.delegate = self
        } else if let createRoomVC = (segue.destination as? UINavigationController)?.topViewController as? CreateRoomViewController {
            createRoomVC.delegate = self
        } else {
            super.prepare(for: segue, sender: sender)
        }
    }

    @IBAction func didTapCreateButton(_ sender: UIBarButtonItem) {
        let actions = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actions.addAction(UIAlertAction(title: "Create Community", style: .default, handler: { (action) in
            self.performSegue(withIdentifier: "CreateCommunitySegue", sender: self)
        }))
        actions.addAction(UIAlertAction(title: "Create Room", style: .default, handler: { (action) in
            self.performSegue(withIdentifier: "CreateRoomSegue", sender: self)
        }))
        actions.addAction(UIAlertAction(title: "Chat with Someone", style: .default, handler: { (action) in
            self.rootViewController?.navigateToPeopleSearch()
        }))
        actions.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        actions.popoverPresentationController?.barButtonItem = sender
        present(actions, animated: true, completion: nil)
    }

    func didCreateRoom(withName name: String) {
        rootViewController?.navigateTo(roomUri: name, sender: self, completionHandler: nil)
    }

    func didIntendToCreateCommunity(_ createRoomViewController: CreateRoomViewController) {
        performSegue(withIdentifier: "CreateCommunitySegue", sender: self)
    }

    func didCreateRoom(_ createRoomViewController: CreateRoomViewController, roomId: String) {
        rootViewController?.navigateTo(roomId: roomId, sender: self)
    }

    @IBAction func onPullToRefresh(_ sender: UIRefreshControl) {
        bayeuxService.requestSnapshot()
    }

    func didFinishRefresh() {
        refreshControl?.endRefreshing()
        viewWhenEmpty = hintView
        if (tableView.backgroundView != nil && tableView.backgroundView != hintView) {
            (tabBarController as! GitterTabBarController).switchTabs(.search)
            // change the empty view shown to the user
            tableView.backgroundView = viewWhenEmpty
        }
    }

    private func configureCell(_ cell: AvatarWithBadgeCell, room: Room) {
        AvatarUtils.sharedInstance.configure(cell.avatar, room: room, size: AvatarWithBadgeCell.avatarHeight)
        cell.mainLabel.text = room.name
        cell.sideLabel.text = room.oneToOne ? "@" + room.uri : nil
        cell.bottomLabel.text = nil
        if (room.intMentions > 0) {
            cell.badgeType = .mention
            cell.badgeLabel.text = "@"
        } else if (room.intUnreadItems > 0) {
            cell.badgeType = .unread
            cell.badgeLabel.text = room.intUnreadItems > 99 ? "99+" : String(room.intUnreadItems)
        } else if (room.activity && room.lurk) {
            cell.badgeType = .activityIndicator
            cell.badgeLabel.text = nil
        } else {
            cell.badgeType = .none
            cell.badgeLabel.text = nil
        }
    }
}
