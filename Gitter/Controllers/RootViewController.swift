import UIKit
import CoreData

class RootViewController: UISplitViewController, UISplitViewControllerDelegate {

    private let appSettings = TRAppSettings.sharedInstance()
    private let notificationCenter = NotificationCenter.default
    private let viewControllerCache = NSCache<NSString, RoomViewController>()
    private let restService = RestService()

    override func loadView() {
        super.loadView()

        notificationCenter.addObserver(self, selector: #selector(self.onPushNotification), name: NSNotification.Name(rawValue: "GitterPushNotification"), object: nil)
    }

    deinit {
        notificationCenter.removeObserver(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.preferredDisplayMode = .allVisible
        self.preferredPrimaryColumnWidthFraction = 1/3
        self.minimumPrimaryColumnWidth = 320
        self.maximumPrimaryColumnWidth = 375
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if (!(appSettings?.notificationAccessRequested())!) {
            showNotificationsRequest()
        }
    }

    func onPushNotification(_ notification: Notification) {
        let id = (notification as NSNotification).userInfo!["id"] as! String
        navigateTo(roomId: id, sender: self)
    }

    func navigateTo(roomUri uri: String, sender: AnyObject?, completionHandler: (() -> Void)?) {
        getRoomId(uri) { (id) in
            if let id = id {
                self.navigateTo(roomId: id, sender: sender)
            }
            completionHandler?()
        }
    }

    func navigateTo(roomId id: String, sender: AnyObject?) {
        let roomViewController = getRoomViewController(byId: id)
        showDetailViewController(roomViewController, sender: sender)
    }

    func navigateTo(topicsForGroupId id: String, sender: AnyObject?) {
        let topicsViewController = UIStoryboard(name: "Topics", bundle: Bundle.main).instantiateInitialViewController() as! TopicsViewController
        topicsViewController.groupId = id

        showDetailViewController(topicsViewController, sender: sender)
    }

    func navigateToPeopleSearch() {
        let tabBarController = viewControllers.first as! GitterTabBarController

        if let searchNavigationController = tabBarController.viewControllers![GitterTab.search.rawValue] as? UINavigationController,
           let searchController = searchNavigationController.viewControllers.first as? SearchViewController {

            searchController.showUserSearchOnAppear = true
            searchNavigationController.popToRootViewController(animated: false)
            tabBarController.switchTabs(.search)
        }
    }

    func getRoomViewController(byId id: String) -> RoomViewController {
        let controller: RoomViewController
        if let cachedController = viewControllerCache.object(forKey: id as NSString) {
            controller = cachedController
        } else {
            controller = (storyboard?.instantiateViewController(withIdentifier: "RoomViewContoller"))! as! RoomViewController
            viewControllerCache.setObject(controller, forKey: id as NSString)
        }

        controller.roomId = id
        controller.restService = restService

        return controller
    }

    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {

        if  let masterNavigationController = primaryViewController as? UINavigationController,
            let detailNavigationController = secondaryViewController as? UINavigationController {

            if getRoomViewController(detailNavigationController, atIndex: 0) != nil {
                // dont bother saving if the detail view controller had no content
                let newNavStack = [masterNavigationController.viewControllers.first!] + detailNavigationController.viewControllers
                masterNavigationController.setViewControllers(newNavStack, animated: false)
            }
        }

        // tell superclass that we have handled the collapse
        return true
    }

    func splitViewController(_ splitViewController: UISplitViewController, separateSecondaryFrom primaryViewController: UIViewController) -> UIViewController? {

        let detailNavigationController = getOrCreateDetailNavigationController()

        // move primary navigation stack onto secondary naviagation stack if a room is on the stack
        if let masterNavigationController = primaryViewController as? UINavigationController
            , getRoomViewController(masterNavigationController, atIndex: 1) != nil {

            let roomViewControllerAndChildren = Array(masterNavigationController.viewControllers.dropFirst())

            masterNavigationController.popToRootViewController(animated: false)
            detailNavigationController.setViewControllers(roomViewControllerAndChildren, animated: false)
        }

        return detailNavigationController
    }

    func splitViewController(_ splitViewController: UISplitViewController, showDetail vc: UIViewController, sender: Any?) -> Bool {
        if viewControllers.count > 1 {
            showDetailViewControllerOnIpad(showDetailViewController: vc, sender: sender as AnyObject?)
        } else {
            showDetailViewControllerOnIphone(showDetailViewController: vc, sender: sender as AnyObject?)
        }

        // tell superclass that we have handled the navigation
        return true
    }

    private func showDetailViewControllerOnIphone(showDetailViewController vc: UIViewController, sender: AnyObject?) {
        let senderViewController = sender as? UIViewController
        let vanillaNavigationController = senderViewController?.navigationController
        // search results controller does not have its own navigation controller as it is a modal
        let modalNavigationController = senderViewController?.presentingViewController?.navigationController

        if let navigationController = vanillaNavigationController ?? modalNavigationController {
            // view controller has been sent by normal navigation, keep back stack
            navigationController.pushViewController(vc, animated: true)
        } else {
            // view controller has been sent by push notification, so...

            // 1. show the home tab
            let tabBarController = viewControllers.first as! GitterTabBarController
            tabBarController.switchTabs(.home)

            // 2. show the room, but keep the room list on the back stack
            let navigationController = tabBarController.viewControllers![GitterTab.home.rawValue] as! UINavigationController
            navigationController.popToRootViewController(animated: false)
            navigationController.pushViewController(vc, animated: true)
        }
    }

    private func showDetailViewControllerOnIpad(showDetailViewController vc: UIViewController, sender: AnyObject?) {
        let detailNavigationVC = viewControllers.last as! UINavigationController

        if ((sender as? UIViewController)?.navigationController == detailNavigationVC){
            // view controller has been sent by right panel, keep back stack
            detailNavigationVC.pushViewController(vc, animated: true)
        } else {
            // view controller has been sent by the left panel, wipe out back stack
            detailNavigationVC.setViewControllers([vc], animated: false)
            vc.navigationItem.leftBarButtonItem = displayModeButtonItem
            vc.navigationItem.leftItemsSupplementBackButton = true
        }
    }

    private func showNotificationsRequest() {
        let title = "Push Notifications"
        let message = "Gitter sends you push notifications when other members in your rooms are communicating.\n\n" +
            "We really believe this is beneficial for keeping in touch.\n\n" +
        "We try to keep the volume low, and will never send you any spam."

        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Got it", style: .default, handler: { (UIAlertAction) -> Void in
            // some event magic means that this will trigger an ios permissions request
            self.appSettings?.setNotificationAccessGranted()
        }))

        present(alert, animated: true, completion: nil)
        appSettings?.setNotificationAccessRequested()
    }

    private func getRoomViewController(_ navigationController: UINavigationController, atIndex: Int) -> RoomViewController? {
        if (navigationController.viewControllers.count > atIndex) {
            return navigationController.viewControllers[atIndex] as? RoomViewController
        } else {
            return nil
        }
    }

    private func getOrCreateDetailNavigationController() -> UINavigationController {
        var existingNavigationController: UINavigationController?

        if (viewControllers.count > 1) {
            existingNavigationController = viewControllers[1] as? UINavigationController
        }

        return existingNavigationController ?? storyboard!.instantiateViewController(withIdentifier: "DetailNavigationController") as! UINavigationController
    }

    private func getRoomId(_ uri: String, completionHandler: @escaping (String?) -> Void) {
        getRoomIdFromDB(uri) { (id) in
            if let id = id {
                completionHandler(id)
            } else {
                self.restService.findOrCreateRoom(uri, completionHandler: { (error, id) in
                    LogIfError("failed to create room", error: error)
                    completionHandler(id)
                })
            }
        }
    }

    private func getRoomIdFromDB(_ uri: String, completionHandler: @escaping (String?) -> Void) {
        let context = CoreDataSingleton.sharedInstance.workerManagedObjectContext
        context.perform {
            let fetchRequest = NSFetchRequest<Room>()
            let entity = NSEntityDescription.entity(forEntityName: "Room", in: context)
            fetchRequest.entity = entity
            fetchRequest.predicate = NSPredicate(format: "url == %@", "/" + uri)
            var room: Room?
            do {
                let result = try context.fetch(fetchRequest)
                room = result.last
            } catch {
                LogError("Failed to get room from core data", error: error)
            }

            DispatchQueue.main.async {
                completionHandler(room?.id)
            }
        }
    }
}
