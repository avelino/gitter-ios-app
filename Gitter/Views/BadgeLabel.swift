import UIKit

class BadgeLabel : UILabel {

    var textInsets: UIEdgeInsets = UIEdgeInsets.init(top: 4, left: 4, bottom: 4, right: 4)

    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        var rect = textInsets.apply(bounds)
        rect = super.textRect(forBounds: rect, limitedToNumberOfLines: numberOfLines)
        return textInsets.inverse.apply(rect)
    }

    override func drawText(in rect: CGRect) {
        super.drawText(in: textInsets.apply(rect))
    }
}

extension UIEdgeInsets {
    var inverse: UIEdgeInsets {
        return UIEdgeInsets(top: -top, left: -left, bottom: -bottom, right: -right)
    }
    func apply(_ rect: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(rect, self)
    }
}
