import Foundation

class BayeuxService: NSObject, TREventControllerDelegate {

    private let notificationCenter = NotificationCenter.default
    private var eventController: TREventController?
    private let jsonToDb = JsonToDatabaseHelper()

    private let logIfError: JsonToDatabaseHelper.Callback = { error, didSaveNewData in
        LogIfError("failed to write stream update to db", error: error)
    }

    override init() {
        super.init()

        connect()
        notificationCenter.addObserver(self, selector: #selector(self.disconnect), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.connect), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.disconnect), name: NSNotification.Name.UIApplicationWillTerminate, object: nil)
    }

    deinit {
        notificationCenter.removeObserver(self)
        disconnect()
    }

    func connect() {
        disconnect()
        eventController = TREventController(delegate: self)
    }

    func disconnect() {
        eventController?.disconnect()
        eventController = nil
    }

    func requestSnapshot() {
        eventController?.reconnect()
    }

    func messageReceived(_ messageDict: [AnyHashable: Any]!, channel: String) {
        if (isUserRoomsChannel(channel)) {
            if let operation = messageDict["operation"] as? String, let model = messageDict["model"] as? [String : AnyObject] {
                switch operation {
                case "create":
                    jsonToDb.upsertRoomInUserRoomList(model, completionHandler: logIfError)
                case "update":
                    jsonToDb.upsertRoomInUserRoomList(model, completionHandler: logIfError)
                case "patch":
                    // we cant assume that the room is in the room list
                    // see https://github.com/gitterHQ/gitter/issues/1265
                    jsonToDb.patchRoom(model, completionHandler: logIfError)
                case "remove":
                    jsonToDb.removeRoomFromUserRoomList(model, completionHandler: logIfError)
                default:
                    break
                }
            }
        } else if (isUserChannel(channel)) {
            if let notification = messageDict["notification"] as? String, let troupeId = messageDict["troupeId"] as? String {
                if (notification == "activity") {
                    // the activity live messages are a little bit odd
                    let json: JsonObject = [
                        "id": troupeId as AnyObject,
                        "activity": true as AnyObject
                    ]
                    jsonToDb.patchRoom(json, completionHandler: logIfError)
                }
            }
        }
    }

    func snapshotReceived(_ snapshot: Any!, channel: String) {
        guard (isUserRoomsChannel(channel)) else {
            return
        }

        if let jsonArray = snapshot as? [JsonObject] {
            jsonToDb.updateUserRoomList(jsonArray) { (error, didSaveNewData) in
                LogIfError("failed to save snapshot data", error: error)
                self.notificationCenter.post(name: Notification.Name(rawValue: "snapshotReceived"), object: nil)
            }
        }
    }

    func serverSubscriptionEstablished() {}
    
    func serverSubscriptionDisconnected() {}

    private func isUserRoomsChannel(_ channel: String) -> Bool {
        return channel.hasPrefix("/api/v1/user/") && channel.hasSuffix("/rooms")
    }

    private func isUserChannel(_ channel: String) -> Bool {
        return channel.hasPrefix("/api/v1/user/") && !channel.hasSuffix("/rooms")
    }
}
