import Foundation

class LoginData {

    private let service = "Gitter: https://gitter.im"
    private let keychain = KeychainItemWrapper(identifier: "OAuth", accessGroup: nil)
    private let userDefaults = UserDefaults.standard

    func setLoggedIn(_ userId:String, withToken accessToken:String) {
        setUserId(userId)
        setAccessToken(accessToken)
    }

    func isLoggedIn() -> Bool {
        return getAccessToken() != nil && getUserId() != nil
    }

    func getAccessToken() -> String? {
        if let secValue = keychain?.object(forKey: kSecValueData) as? String {
            if (secValue.isEmpty) {
                return nil
            } else {
                return parse(secValue)
            }
        } else {
            return nil
        }
    }

    func getUserId() -> String? {
        return userDefaults.string(forKey: "userId")
    }

    func clear() {
        keychain?.resetKeychainItem()
        clearUserId()
    }

    private func setAccessToken(_ accessToken: String) {
        keychain?.setObject(service, forKey: kSecAttrService)
        keychain?.setObject("OAuth", forKey: kSecAttrAccount)
        keychain?.setObject(kSecAttrAccessibleAfterFirstUnlockThisDeviceOnly, forKey: kSecAttrAccessible)
        keychain?.setObject(generateGTMOAuth2String(accessToken), forKey: kSecValueData)
    }

    private func setUserId(_ id: String) {
        userDefaults.set(id, forKey: "userId")
    }

    private func clearUserId() {
        userDefaults.removeObject(forKey: "userId")
    }

    private func generateGTMOAuth2String(_ accessToken: String) -> String {
        let scope = "https://gitter.im/login/oauth/callback"
        return "access_token=\(accessToken)&scope=\(urlEncode(scope))&serviceProvider=\(urlEncode(service))"
    }

    private func urlEncode(_ string: String) -> String {
        return string.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }

    private func parse(_ GTMOAuth2String: String) -> String {
        var results = [String: String]()
        for encodedPair in GTMOAuth2String.components(separatedBy: "&") {
            let pair = encodedPair.components(separatedBy: "=");
            let key = pair[0].removingPercentEncoding!
            let value = pair[1].removingPercentEncoding!
            results[key] = value;
        }

        return results["access_token"]!
    }
}
